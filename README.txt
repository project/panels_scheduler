
== Panels Scheduler ==

At present, the scheduler can only schedule panel pages for 'frontpage'.

= Installation =

This module expects the 'panel' node type, specified by the panels_node module, to have a date field. This field should be of type 'Date' and have a machine readable name of 'field_panel_date'.
